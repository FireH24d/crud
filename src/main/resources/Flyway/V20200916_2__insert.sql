

INSERT INTO actdestroy (id, no_act, base, company_unit_id, created_timestamp, created_by, updated_timestamp,
                        updated_by)
VALUES (1, '4', '4', 1, 1, 1, 1, 1),(2, '4', '4', 1, 1, 1, 1, 1),
       (3, 'gg', 'ggg5', 5, 5, 5, 5, 5);

INSERT INTO activityjournal (id, created_by, created_timestamp, event_type, message, message_level, object_id,
                             object_type)
VALUES (1, 1, 1, '1', '1', '1', 1, '1'),
       (2, 2, 2, '2', '2', '2', 2, '2'),
       (3, 3, 3, '3', '3', '3', 3, '3');

INSERT INTO fond (id, created_by, created_timestamp, fond_number, updated_by, updated_timestamp)
VALUES (1, 1, 1, '1', 1, 1),(2, 2, 2, '2', 2, 2), (3, 3, 3, '3', 3, 3);

INSERT INTO company (id, bin, created_by, created_timestamp, fond_id, name_en, name_kz, name_ru, parent_id,
                     updated_by, updated_timestamp)
VALUES (1, '1', 1, 1, 1, '1', '1', '1', 1, 1, 1),(2, '2', 2, 2, 2, '2', '2', '2', 2, 2, 2),(3, '3', 3, 33, 3, '3', '3', '3', 3, 3, 3);

INSERT INTO companyunit (id, code_index, company_id, created_by, created_timestamp, name_en, name_kz, name_ru,
                         parent_id, updated_by, updated_timestamp, year)
VALUES (1, '1', 1, 1, 1, '1', '1', '1', 1, 1, 1, 1),(2, '2', 2, 2, 2, '2', '2', '2', 2, 2, 2, 2), (3, '3', 3, 3, 3, '3', '3', '3', 3, 3, 3, 3);

INSERT INTO location (id, box, columnn, company_unit_id, created_by, created_timestamp, line, row, updated_by,
                      updated_timestamp)
VALUES (1, '1', '1', 1, 1, 1, '1', '1', 1, 1), (2, '2', '2', 2, 2, 2, '2', '2', 2, 2), (3, '3', '3', 3, 3, 3, '3', '3', 3, 3);

INSERT INTO nomenclaturesummary (id, company_unit_id, created_by, created_timestamp, number, updated_by,
                                 updated_timestamp, year)
VALUES (1, 1, 1, 1, '1', 1, 1, 1),(2, 2, 2, 2, '2', 2, 2, 2), (3, 3, 3, 3, '3', 3, 3, 3);

INSERT INTO nomenclature (id, company_unit_id, created_by, created_timestamp, nomenclature_no,
                          nomenclature_summary_id, updated_by, updated_timestamp, year)
VALUES (1, 1, 1, 1, '1', 1, 1, 1, 1),(2, 2, 2, 2, '2', 2, 2, 2, 2),(3, 3, 3, 3, '3', 3, 3, 3, 3);
INSERT INTO notification (id, company_id, company_unit_id, created_timestamp, is_viewed, object_id, object_type,
                          text, title, user_id, viewed_timestamp)
VALUES (1, 1, 1, 1, true, 1, '1', '1', '1', 1, 1), (2, 2, 2, 2, false, 2, '2', '2', '2', 2, 2), (3, 3, 3, 3, false, 3, '3', '3', '3', 3, 3);

INSERT INTO record (id, company_unit_id, created_by, created_timestamp, number, type, updated_by,
                    updated_timestamp)
VALUES (1, 1, 1, 1, '1', '1', 1, 1), (2, 2, 2, 2, '2', '2', 2, 2),(3, 3, 3, 3, '3', '3', 3, 3);


INSERT INTO searchkey (id, company_unit_id, created_by, created_timestamp, name, updated_by, updated_timestamp)
VALUES (1, 1, 1, 1, '1', 1, 1), (2, 2, 2, 2, '2', 2, 2),(3, 3, 3, 3, '3', 3, 3);



INSERT INTO tempfiles (id, file_binary, file_binary_byte)
VALUES (1, '1', null), (2, '2', null), (3, '3', null);
INSERT INTO file (id, created_by, created_timestamp, file_binary_id, hash, is_deleted, name, page_count, size,
                  type, updated_by, updated_timestamp)
VALUES (1, 1, 1, 1, '1', true, '1', 1, 1, '1', 1, 1),(2, 2, 2, 2, '2', false, '2', 2, 2, '2', 2, 2),(3, 3, 3, 3, '3', false, '3', 3, 3, '3', 3, 3);

INSERT INTO filerouting (id, file_id, table_id, table_name, type)
VALUES (1, 1, 1, '1', '1'),(2, 2, 2, '2', '2'), (3, 3, 3, '3', '3');
INSERT INTO autorization (id, login, email, password, role, forgot_password_key, forgot_password_key_time,
                          company_unit_id)
VALUES (1, '1', '1', '1', '1', '1', 1, 1),
       (2, '2', '2', '2', '2', '2', 2, 2),
       (3, '3', '3', '3', '3', '3', 3, 3);
INSERT INTO userr (id, auth_id, company_unit_id, created_by, created_timestamp, iin, is_activated, is_active,
                   lastlogin_time, name, password, secondname, status, surname, updated_by, updated_timestamp)
VALUES (1, 1, 1, 1, 1, '1', true, true, 1, '1', '1', '1', '1', '1', 1, 1), (2, 2, 2, 2, 2, '2', false, true, 2, '2', '2', '2', '2', '2', 2, 2), (3, 3, 3, 3, 3, '3', true, true, 3, '3', '3', '3', '3', '3', 3, 3);


INSERT INTO casee (id, eds, act_destroy_id, address_case_blockchain, case_en, case_kz, case_ru, changed_bywho,
                   created_bywho, date_adding_blockchain, date_change, date_crating, deleting_sign, finish_data,
                   hash, id_version, index_case_id, is_eds, is_active, location_id, no_case, no_pages, no_tome,
                   note, restricted_flag, sending_sign_naf, sign_id, start_data, structural_separation_id,
                   version)
VALUES (1, '1', 1, '1', '1', '1', '1', 1, 1, 1, 1, 1, true, 1, '1', '1', 1, true, true, 1, '1', 1, '1', '1', true, true,
        1, 1, 1, 1),(2, '2', 2, '2', '2', '2', '2', 2, 2, 2, 2, 2, true, 2, '2', '2', 2, true, false, 2, null, 2, '2', '2', false,
                     true, 2, 2, 2, 2),(3, '3', 3, '3', '3', '3', '3', 3, 3, 3, 3, 3, false, 3, '3', '3', 3, false, true, 3, '3', 3, '3', '3', false,
                                        true, 3, 3, 3, 3);
INSERT INTO searchkeyrouting (id, search_key_id, table_id, table_name, type)
VALUES (1, 1, 1, '1', '1'), (2, 2, 2, '2', '2'), (3, 3, 3, '3', '3');


INSERT INTO catalog (id, company_unit_id, created_by, created_timestamp, name_en, name_kz, name_ru, parent_id,
                     updated_by, updated_timestamp)
VALUES (1, 1, 1, 1, '1', '1', '1', 1, 1, 1),(2, 2, 2, 2, '2', '2', '2', 2, 2, 2),(3, 3, 3, 3, '3', '3', '3', 3, 3, 3);


INSERT INTO catalogcase (id, case_id, catalog_id, company_unit_id, created_by, created_timestamp, updated_by,
                         updated_timestamp)
VALUES (1, 1, 1, 1, 1, 1, 1, 1),(2, 2, 2, 2, 2, 2, 2, 2),(3, 3, 3, 3, 3, 3, 3, 3);
INSERT INTO caseindex (id, case_index, company_unit_id, created_by, created_timestamp, nomenclature_id, note,
                       storage_type, storage_year, title_en, title_kz, title_ru, updated_by, updated_timestamp)
VALUES (1, '1', 1, 1, 1, 1, '1', 1, 1, '1', '1', '1', 1, 1),(2, '2', 2, 2, 2, 2, '2', 2, 2, '2', '2', '2', 2, 2),
       (3, '3', 3, 3, 3, 3, '3', 3, 3, '3', '3', '3', 3, 3);

INSERT INTO request (id, case_id, case_index_id, comment, company_unit_id, created_type, declinenote, favorite,
                     from_request_id, request_user_id, response_user_id, sharefinish, sharestart, status,
                     timestamp, update_by, update_timestamp)
VALUES (1, 1, 1, '1', 1, '1', '1', false, 1, 1, 1, 1, 1, '1', 1, 1, 1), (2, 2, 2, '2', 2, '2', '2', true, 2, 2, 2, 2, 2, '2', 2, 2, 2)

     ,(3, 3, 3, '3', 3, '3', '3', false, 3, 3, 3, 3, 3, '3', 3, 3, 3);

INSERT INTO statusrequesthistory (id, created_by, created_timestamp, request_id, status, updated_by,
                                  updated_timestamp)
VALUES (1, 1, 1, 1, '1', 1, 1), (2, 2, 2, 2, '2', 2, 2), (3, 3, 3, 3, '3', 3, 3);
INSERT INTO share (id, note, receiver_id, request_id, sender_id, share_timestamp)
VALUES (1, '1', 1, 1, 1, 1),(2, '2', 2, 2, 2, 2),(3, '3', 3, 3, 3, 3);







