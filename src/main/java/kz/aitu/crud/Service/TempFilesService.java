package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Entity.TempFiles;
import kz.aitu.crud.Repository.RecordRepository;
import kz.aitu.crud.Repository.TempFilesRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class TempFilesService {

    TempFilesRepository tempFilesRepository;

    public TempFilesService(TempFilesRepository tempFilesRepository) {
        this.tempFilesRepository = tempFilesRepository;
    }

    public List<TempFiles> getAllTempFiles(){
        return (List<TempFiles>) tempFilesRepository.findAll();
    }
    public Optional<TempFiles> getTempFiles(long id) {
        return tempFilesRepository.findById(id);
    }

    public void deleteTempFiles(long id){
        tempFilesRepository.deleteById(id);
    }
    public TempFiles updateTempFiles(@RequestBody TempFiles tempFiles){
        return  tempFilesRepository.save(tempFiles);
    }
}
