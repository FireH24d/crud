package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.File;
import kz.aitu.crud.Entity.Fond;
import kz.aitu.crud.Repository.FileRepository;
import kz.aitu.crud.Repository.FondRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class FileService {
    FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAllFile(){
        return (List<File>) fileRepository.findAll();
    }
    public Optional<File> getFile(long id) {
        return fileRepository.findById(id);
    }

    public void deleteFile(long id){
        fileRepository.deleteById(id);
    }
    public File updateFile(@RequestBody File file){
        return  fileRepository.save(file);
    }
}
