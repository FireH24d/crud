package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Notification;
import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Repository.NotificationRepository;
import kz.aitu.crud.Repository.RecordRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class NotificationService {
    NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<Notification> getAllNotification(){
        return (List<Notification>) notificationRepository.findAll();
    }
    public Optional<Notification> getNotification(long id) {
        return notificationRepository.findById(id);
    }

    public void deleteNotification(long id){
        notificationRepository.deleteById(id);
    }
    public Notification updateNotification(@RequestBody Notification notification){
        return  notificationRepository.save(notification);
    }
}
