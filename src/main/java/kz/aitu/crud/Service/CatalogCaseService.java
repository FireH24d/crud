package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Case;
import kz.aitu.crud.Entity.CatalogCase;
import kz.aitu.crud.Repository.CaseRepository;
import kz.aitu.crud.Repository.CatalogCaseRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CatalogCaseService {
    CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }
    public List<CatalogCase> getAllCatalogCase(){
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }
    public Optional<CatalogCase> getCatalogCase(long id) {
        return catalogCaseRepository.findById(id);
    }

    public void deleteCatalogCase(long id){
        catalogCaseRepository.deleteById(id);
    }
    public CatalogCase updateCatalogCase(@RequestBody CatalogCase a){
        return  catalogCaseRepository.save(a);
    }
}
