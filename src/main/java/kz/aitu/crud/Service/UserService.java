package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.User;
import kz.aitu.crud.Repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class UserService {

    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUser(){
        return (List<User>) userRepository.findAll();
    }
    public Optional<User> getUser(long id) {
        return userRepository.findById(id);
    }

    public void deleteUser(long id){
        userRepository.deleteById(id);
    }
    public User updateUser(@RequestBody User user){
        return  userRepository.save(user);
    }
}
