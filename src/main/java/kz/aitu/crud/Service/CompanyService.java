package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Company;
import kz.aitu.crud.Repository.CompanyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CompanyService {
    CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }
    public List<Company> getAllCompany(){
        return (List<Company>) companyRepository.findAll();
    }
    public Optional<Company> getCompany(long id) {
        return companyRepository.findById(id);
    }

    public void deleteCompany(long id){
        companyRepository.deleteById(id);
    }
    public Company updateCompany(@RequestBody Company company){
        return  companyRepository.save(company);
    }
}
