package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.ActDestroy;
import kz.aitu.crud.Repository.ActDestroyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class ActDestroyService {
    ActDestroyRepository actDestroyRepository;

    public ActDestroyService(ActDestroyRepository actDestroyRepository) {
        this.actDestroyRepository = actDestroyRepository;
    }
    public List<ActDestroy> getAllActDestroy(){
        return (List<ActDestroy>) actDestroyRepository.findAll();
    }
    public Optional<ActDestroy> getActDestroy(long id) {
        return actDestroyRepository.findById(id);
    }

    public void deleteActDestroy(long id){
        actDestroyRepository.deleteById(id);
    }
    public ActDestroy updateArcDestroy(@RequestBody ActDestroy actDestroy){
        return  actDestroyRepository.save(actDestroy);
    }
}
