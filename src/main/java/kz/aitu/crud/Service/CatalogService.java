package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Catalog;
import kz.aitu.crud.Repository.CatalogRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CatalogService {
    CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }
    public List<Catalog> getAllCatalog(){
        return (List<Catalog>) catalogRepository.findAll();
    }
    public Optional<Catalog> getCatalog(long id) {
        return catalogRepository.findById(id);
    }

    public void deleteCatalog(long id){
        catalogRepository.deleteById(id);
    }
    public Catalog updateCatalog(@RequestBody Catalog catalog){
        return  catalogRepository.save(catalog);
    }
}
