package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.CompanyUnit;
import kz.aitu.crud.Repository.CompanyUnitRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CompanyUnitService {
    CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAllCompanyUnit(){
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }
    public Optional<CompanyUnit> getCompanyUnit(long id) {
        return companyUnitRepository.findById(id);
    }

    public void deleteCompanyUnit(long id){
        companyUnitRepository.deleteById(id);
    }
    public CompanyUnit updateCompanyUnit(@RequestBody CompanyUnit companyUnit){
        return  companyUnitRepository.save(companyUnit);
    }
}
