package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.SearchKey;
import kz.aitu.crud.Entity.SearchKeyRouting;
import kz.aitu.crud.Repository.SearchKeyRepository;
import kz.aitu.crud.Repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class SearchKeyRoutingService {
    SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAllSearchKeyRouting(){
        return (List<SearchKeyRouting>) searchKeyRoutingRepository.findAll();
    }
    public Optional<SearchKeyRouting> getSearchKeyRouting(long id) {
        return searchKeyRoutingRepository.findById(id);
    }
    public void deleteSearchKeyRouting(long id){
        searchKeyRoutingRepository.deleteById(id);
    }
    public SearchKeyRouting updateSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return  searchKeyRoutingRepository.save(searchKeyRouting);
    }
}
