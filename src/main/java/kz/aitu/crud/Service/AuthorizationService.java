package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Authorization;
import kz.aitu.crud.Repository.AuthorizationRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorizationService {
    AuthorizationRepository authorizationRepository;

    public AuthorizationService(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    public List<Authorization> getAllAuthorization(){
        return (List<Authorization>) authorizationRepository.findAll();
    }
    public Optional<Authorization> getAuthorization(long id) {
        return authorizationRepository.findById(id);
    }

    public void deleteAuthorization(long id){
        authorizationRepository.deleteById(id);
    }
    public Authorization updateAuthorization(@RequestBody Authorization authorization){
        return  authorizationRepository.save(authorization);
    }
}
