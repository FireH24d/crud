package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Share;
import kz.aitu.crud.Entity.StatusRequestHistory;
import kz.aitu.crud.Repository.ShareRepository;
import kz.aitu.crud.Repository.StatusRequestHistoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class StatusRequestHistoryService {
    StatusRequestHistoryRepository statusRequestHistoryRepository;

    public StatusRequestHistoryService(StatusRequestHistoryRepository statusRequestHistoryRepository) {
        this.statusRequestHistoryRepository = statusRequestHistoryRepository;
    }
    public List<StatusRequestHistory> getAllStatusRequestHistory(){
        return (List<StatusRequestHistory>) statusRequestHistoryRepository.findAll();
    }
    public Optional<StatusRequestHistory> getStatusRequestHistory(long id) {
        return statusRequestHistoryRepository.findById(id);
    }
    public void deleteStatusRequestHistory(long id){
        statusRequestHistoryRepository.deleteById(id);
    }
    public StatusRequestHistory updateStatusRequestHistory(@RequestBody StatusRequestHistory statusRequestHistory){
        return  statusRequestHistoryRepository.save(statusRequestHistory);
    }
}
