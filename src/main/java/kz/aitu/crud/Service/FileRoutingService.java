package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.File;
import kz.aitu.crud.Entity.FileRouting;
import kz.aitu.crud.Repository.FileRepository;
import kz.aitu.crud.Repository.FileRoutingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class FileRoutingService {
    FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAllFileRouting(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }
    public Optional<FileRouting> getFileRouting(long id) {
        return fileRoutingRepository.findById(id);
    }

    public void deleteFileRouting(long id){
        fileRoutingRepository.deleteById(id);
    }
    public FileRouting updateFileRouting(@RequestBody FileRouting fileRouting){
        return  fileRoutingRepository.save(fileRouting);
    }
}
