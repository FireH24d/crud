package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Case;
import kz.aitu.crud.Entity.Catalog;
import kz.aitu.crud.Repository.CaseRepository;
import kz.aitu.crud.Repository.CatalogRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CaseService {
    CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }
    public List<Case> getAllCase(){
        return (List<Case>) caseRepository.findAll();
    }
    public Optional<Case> getCase(long id) {
        return caseRepository.findById(id);
    }

    public void deleteCase(long id){
        caseRepository.deleteById(id);
    }
    public Case updateCase(@RequestBody Case a){
        return  caseRepository.save(a);
    }
}
