package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.SearchKey;
import kz.aitu.crud.Entity.Share;
import kz.aitu.crud.Repository.SearchKeyRepository;
import kz.aitu.crud.Repository.ShareRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ShareService {
    ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAllShare(){
        return (List<Share>) shareRepository.findAll();
    }
    public Optional<Share> getShare(long id) {
        return shareRepository.findById(id);
    }
    public void deleteShare(long id){
        shareRepository.deleteById(id);
    }
    public Share updateShare(@RequestBody Share share){
        return  shareRepository.save(share);
    }
}
