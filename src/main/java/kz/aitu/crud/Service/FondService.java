package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.CompanyUnit;
import kz.aitu.crud.Entity.Fond;
import kz.aitu.crud.Repository.CompanyUnitRepository;
import kz.aitu.crud.Repository.FondRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FondService {
    FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAllFond(){
        return (List<Fond>) fondRepository.findAll();
    }
    public Optional<Fond> getFond(long id) {
        return fondRepository.findById(id);
    }

    public void deleteFond(long id){
        fondRepository.deleteById(id);
    }
    public Fond updateFond(@RequestBody Fond fond){
        return  fondRepository.save(fond);
    }
}
