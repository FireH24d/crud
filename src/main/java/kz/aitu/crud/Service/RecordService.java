package kz.aitu.crud.Service;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Repository.LocationRepository;
import kz.aitu.crud.Repository.RecordRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class RecordService {
    RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAllRecord(){
        return (List< Record>) recordRepository.findAll();
    }
    public Optional<Record> getRecord(long id) {
        return recordRepository.findById(id);
    }

    public void deleteRecord(long id){
        recordRepository.deleteById(id);
    }
    public Record updateRecord(@RequestBody Record record){
        return  recordRepository.save(record);
    }

}
