package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {

}
