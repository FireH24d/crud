package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Share;
import kz.aitu.crud.Entity.StatusRequestHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRequestHistoryRepository extends CrudRepository<StatusRequestHistory, Long> {

}