package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.SearchKey;
import kz.aitu.crud.Entity.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting, Long> {

}
