package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.NomenclatureSummary;
import kz.aitu.crud.Entity.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {

}
