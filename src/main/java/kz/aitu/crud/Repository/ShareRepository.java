package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.SearchKeyRouting;
import kz.aitu.crud.Entity.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {

}