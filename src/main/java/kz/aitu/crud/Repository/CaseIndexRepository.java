package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Case;
import kz.aitu.crud.Entity.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {

}