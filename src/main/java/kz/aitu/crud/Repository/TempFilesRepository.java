package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Entity.TempFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempFilesRepository extends CrudRepository<TempFiles, Long> {

}
