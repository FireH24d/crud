package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.ActDestroy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActDestroyRepository extends CrudRepository<ActDestroy, Long> {

}