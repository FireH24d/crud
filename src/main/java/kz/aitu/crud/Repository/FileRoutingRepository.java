package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.File;
import kz.aitu.crud.Entity.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {

}