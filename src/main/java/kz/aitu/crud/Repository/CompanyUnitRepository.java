package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {

}
