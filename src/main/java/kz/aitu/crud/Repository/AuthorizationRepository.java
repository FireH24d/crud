package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization, Long> {

}
