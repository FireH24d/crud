package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.File;
import kz.aitu.crud.Entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {

}
