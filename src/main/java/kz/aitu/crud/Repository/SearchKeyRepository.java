package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Request;
import kz.aitu.crud.Entity.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey, Long> {

}
