package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Entity.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {

}
