package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Location;
import kz.aitu.crud.Entity.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {

}
