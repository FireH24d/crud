package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends CrudRepository<Case, Long> {

}