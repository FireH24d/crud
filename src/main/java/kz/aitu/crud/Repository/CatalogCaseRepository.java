package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Case;
import kz.aitu.crud.Entity.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {

}