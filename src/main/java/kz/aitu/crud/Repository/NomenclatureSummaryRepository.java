package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Nomenclature;
import kz.aitu.crud.Entity.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {

}