package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {

}
