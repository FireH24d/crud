package kz.aitu.crud.Repository;

import kz.aitu.crud.Entity.Fond;
import kz.aitu.crud.Entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {

}