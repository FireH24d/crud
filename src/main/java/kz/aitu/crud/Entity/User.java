package kz.aitu.crud.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "userr")
public class User {
    @Id
    private long id;
    @Column
    private long auth_id;
    private String name;
    private String surname;
    private String secondname;
    private String status;
    private long company_unit_id;
    private String password;
    private long lastlogin_time;
    private String iin;
    private boolean is_active;
    private boolean is_activated;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;
}
