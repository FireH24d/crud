package kz.aitu.crud.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "casee")
public class Case {
    @Id
    private long id;
    @Column
    private String no_case;
    private String no_tome;
    private String case_ru;
    private String case_kz;
    private String case_en;
    private long start_data;
    private long finish_data;
    private long no_pages;
    private boolean is_EDS;
    private String EDS;
    private boolean sending_sign_NAF;
    private boolean deleting_sign;
    private boolean restricted_flag;
    private String hash;
    private int version;
    private String id_version;
    private boolean is_active;
    private String note;
    private long location_id;
    private long index_case_id;
    private long sign_id;
    private long act_destroy_id;
    private long structural_separation_id;
    private String address_case_blockchain;
    private long date_adding_blockchain;
    private long date_crating;
    private long created_bywho;
    private long date_change;
    private long changed_bywho;
}
