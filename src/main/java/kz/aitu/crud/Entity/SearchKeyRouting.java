package kz.aitu.crud.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "searchkeyrouting")
public class SearchKeyRouting {
    @Id
    private long id;
    @Column
    private long search_key_id;
    private String table_name;
    private long table_id;
    private String type;
}
