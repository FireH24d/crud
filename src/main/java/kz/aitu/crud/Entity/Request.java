package kz.aitu.crud.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "request")
public class Request {
    @Id
    private long id;
    @Column
    private long request_user_id;
    private long response_user_id;
    private long case_id;
    private long case_index_id;
    private String created_type;
    private String comment;
    private String status;
    private long timestamp;
    private long sharestart;
    private long sharefinish;
    private boolean favorite;
    private long update_timestamp;
    private long update_by;
    private String declinenote;
    private long company_unit_id;
    private long from_request_id;
}
