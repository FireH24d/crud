package kz.aitu.crud.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = ("autorization"))
public class Authorization {
    @Id
    private long id;
    @Column
    private String login;
    private String email;
    private String password;
    private String role;
    private String forgot_password_key;
    private long forgot_password_key_time;
    private long company_unit_id;
}
