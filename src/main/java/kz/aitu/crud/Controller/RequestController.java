package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Entity.Request;
import kz.aitu.crud.Service.RecordService;
import kz.aitu.crud.Service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }
    @GetMapping(path="/Request")
    public ResponseEntity<?> getAllRequest(){
        return ResponseEntity.ok(requestService.getAllRequest());
    }
    @GetMapping(path="/Request/{id}")
    public ResponseEntity<?> getRequest(@PathVariable long id){
        return ResponseEntity.ok(requestService.getRequest(id));}

    @DeleteMapping(path="/Request/{id}")
    public String deleteRequest(@PathVariable int id){
        requestService.deleteRequest(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Request", method= RequestMethod.PUT)
    public ResponseEntity<?> updateRequest(@RequestBody Request request){
        return ResponseEntity.ok(requestService.updateRequest(request));
    }
}
