package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.SearchKey;
import kz.aitu.crud.Entity.SearchKeyRouting;
import kz.aitu.crud.Service.SearchKeyRoutingService;
import kz.aitu.crud.Service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }
    @GetMapping(path="/SearchKeyRouting")
    public ResponseEntity<?> getAllSearchKeyRouting(){
        return ResponseEntity.ok(searchKeyRoutingService.getAllSearchKeyRouting());
    }
    @GetMapping(path="/SearchKeyRouting/{id}")
    public ResponseEntity<?> getSearchKeyRouting(@PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.getSearchKeyRouting(id));}

    @DeleteMapping(path="/SearchKeyRouting/{id}")
    public String deleteSearchKeyRouting(@PathVariable int id){
        searchKeyRoutingService.deleteSearchKeyRouting(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/SearchKeyRouting", method= RequestMethod.PUT)
    public ResponseEntity<?> updateSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingService.updateSearchKeyRouting(searchKeyRouting));
    }
}
