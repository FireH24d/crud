package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.CompanyUnit;
import kz.aitu.crud.Service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }
    @GetMapping(path="/CompanyUnit")
    public ResponseEntity<?> getAllCompanyUnit(){
        return ResponseEntity.ok(companyUnitService.getAllCompanyUnit());
    }
    @GetMapping(path="/CompanyUnit/{id}")
    public ResponseEntity<?> getCompanyUnit(@PathVariable long id){
        return ResponseEntity.ok(companyUnitService.getCompanyUnit(id));}

    @DeleteMapping(path="/CompanyUnit/{id}")
    public String deleteCompanyUnit(@PathVariable int id){
        companyUnitService.deleteCompanyUnit(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/CompanyUnit", method= RequestMethod.PUT)
    public ResponseEntity<?> updateCompanyUnit(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok(companyUnitService.updateCompanyUnit(companyUnit));
    }
}
