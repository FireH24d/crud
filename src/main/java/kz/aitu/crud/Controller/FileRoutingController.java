package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.File;
import kz.aitu.crud.Entity.FileRouting;
import kz.aitu.crud.Service.FileRoutingService;
import kz.aitu.crud.Service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }
    @GetMapping(path="/FileRouting")
    public ResponseEntity<?> getAllFileRouting(){
        return ResponseEntity.ok(fileRoutingService.getAllFileRouting());
    }
    @GetMapping(path="/FileRouting/{id}")
    public ResponseEntity<?> getFileRouting(@PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.getFileRouting(id));}

    @DeleteMapping(path="/FileRouting/{id}")
    public String deleteFileRouting(@PathVariable int id){
        fileRoutingService.deleteFileRouting(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/FileRouting", method= RequestMethod.PUT)
    public ResponseEntity<?> updateFileRouting(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.updateFileRouting(fileRouting));
    }
}
