package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Notification;
import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Service.NotificationService;
import kz.aitu.crud.Service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    @GetMapping(path="/Notification")
    public ResponseEntity<?> getAllNotification(){
        return ResponseEntity.ok(notificationService.getAllNotification());
    }
    @GetMapping(path="/Notification/{id}")
    public ResponseEntity<?> getNotification(@PathVariable long id){
        return ResponseEntity.ok(notificationService.getNotification(id));}

    @DeleteMapping(path="/Notification/{id}")
    public String deleteNotification(@PathVariable int id){
        notificationService.deleteNotification(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Notification", method= RequestMethod.PUT)
    public ResponseEntity<?> updateNotification(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.updateNotification(notification));
    }
}
