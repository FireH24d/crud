package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.ActivityJournal;
import kz.aitu.crud.Service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }
    @GetMapping(path="/ActivityJournal")
    public ResponseEntity<?> getAllActivityJournal(){
        return ResponseEntity.ok(activityJournalService.getAllActivityJournal());
    }
    @GetMapping(path="/ActivityJournal/{id}")
    public ResponseEntity getActivityJournal(@PathVariable long id){
        return ResponseEntity.ok(activityJournalService.getActivityJournal(id));}

    @DeleteMapping(path="/ActivityJournal/{id}")
    public String deleteActivityJournal(@PathVariable int id){
        activityJournalService.deleteActivityJournal(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/ActivityJournal", method= RequestMethod.PUT)
    public ResponseEntity<?> updateActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.updateActivityJournal(activityJournal));
    }
}
