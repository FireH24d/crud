package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Service.LocationService;
import kz.aitu.crud.Service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }
    @GetMapping(path="/Record")
    public ResponseEntity<?> getAllRecord(){
        return ResponseEntity.ok(recordService.getAllRecord());
    }
    @GetMapping(path="/Record/{id}")
    public ResponseEntity<?> getRecord(@PathVariable long id){
        return ResponseEntity.ok(recordService.getRecord(id));}

    @DeleteMapping(path="/Record/{id}")
    public String deleteRecord(@PathVariable int id){
        recordService.deleteRecord(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Record", method= RequestMethod.PUT)
    public ResponseEntity<?> updateRecord(@RequestBody Record record){
        return ResponseEntity.ok(recordService.updateRecord(record));
    }
}
