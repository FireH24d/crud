package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.ActDestroy;
import kz.aitu.crud.Service.ActDestroyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActDestroyController {
    private final ActDestroyService actDestroyService;

    public ActDestroyController(ActDestroyService actDestroyService) {
        this.actDestroyService = actDestroyService;
    }
    @GetMapping(path="/ActDestroy")
    public ResponseEntity<?> getAllActDestroy(){
        return ResponseEntity.ok(actDestroyService.getAllActDestroy());
    }
    @GetMapping(path="/ActDestroy/{id}")
    public ResponseEntity<?> getActDestroy(@PathVariable long id){
        return ResponseEntity.ok(actDestroyService.getActDestroy(id));}

    @DeleteMapping(path="/ActDestroy/{id}")
    public String deleteActDestroy(@PathVariable int id){
        actDestroyService.deleteActDestroy(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/ActDestroy", method=RequestMethod.PUT)
    public ResponseEntity<?> updateActDestroy(@RequestBody ActDestroy actDestroy){
        return ResponseEntity.ok(actDestroyService.updateArcDestroy(actDestroy));
    }
}
