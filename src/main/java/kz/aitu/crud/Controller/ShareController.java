package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Share;
import kz.aitu.crud.Entity.TempFiles;
import kz.aitu.crud.Service.ShareService;
import kz.aitu.crud.Service.TempFilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }
    @GetMapping(path="/Share")
    public ResponseEntity<?> getAllShare(){
        return ResponseEntity.ok(shareService.getAllShare());
    }
    @GetMapping(path="/Share/{id}")
    public ResponseEntity<?> getShare(@PathVariable long id){
        return ResponseEntity.ok(shareService.getShare(id));}

    @DeleteMapping(path="/Share/{id}")
    public String deleteShare(@PathVariable int id){
        shareService.deleteShare(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Share", method= RequestMethod.PUT)
    public ResponseEntity<?> updateShare(@RequestBody Share share){
        return ResponseEntity.ok(shareService.updateShare(share));
    }
}
