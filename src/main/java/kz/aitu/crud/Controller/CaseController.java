package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Case;
import kz.aitu.crud.Entity.Catalog;
import kz.aitu.crud.Service.CaseService;
import kz.aitu.crud.Service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {
    private final CaseService caseService;

    public CaseController(CaseService caseService) {
        this.caseService = caseService;
    }
    @GetMapping(path="/Case")
    public ResponseEntity<?> getAllCase(){
        return ResponseEntity.ok(caseService.getAllCase());
    }
    @GetMapping(path="/Case/{id}")
    public ResponseEntity<?> getCase(@PathVariable long id){
        return ResponseEntity.ok(caseService.getCase(id));}

    @DeleteMapping(path="/Case/{id}")
    public String deleteCase(@PathVariable int id){
        caseService.deleteCase(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Case", method= RequestMethod.PUT)
    public ResponseEntity<?> updateCase(@RequestBody Case a){
        return ResponseEntity.ok(caseService.updateCase(a));
    }
}
