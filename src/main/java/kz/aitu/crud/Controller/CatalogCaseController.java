package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.CatalogCase;
import kz.aitu.crud.Service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }
    @GetMapping(path="/CatalogCase")
    public ResponseEntity<?> getAllCatalogCase(){
        return ResponseEntity.ok(catalogCaseService.getAllCatalogCase());
    }
    @GetMapping(path="/CatalogCase/{id}")
    public ResponseEntity<?> getCatalogCase(@PathVariable long id){
        return ResponseEntity.ok(catalogCaseService.getCatalogCase(id));}

    @DeleteMapping(path="/CatalogCase/{id}")
    public String deleteCatalogCase(@PathVariable int id){
        catalogCaseService.deleteCatalogCase(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/CatalogCase", method= RequestMethod.PUT)
    public ResponseEntity<?> updateCatalogCase(@RequestBody CatalogCase a){
        return ResponseEntity.ok(catalogCaseService.updateCatalogCase(a));
    }
}
