package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Record;
import kz.aitu.crud.Entity.TempFiles;
import kz.aitu.crud.Service.RecordService;
import kz.aitu.crud.Service.TempFilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempFilesController {
    private final TempFilesService tempFilesService;

    public TempFilesController(TempFilesService tempFilesService) {
        this.tempFilesService = tempFilesService;
    }
    @GetMapping(path="/TempFiles")
    public ResponseEntity<?> getAllTempFiles(){
        return ResponseEntity.ok(tempFilesService.getAllTempFiles());
    }
    @GetMapping(path="/TempFiles/{id}")
    public ResponseEntity<?> getTempFiles(@PathVariable long id){
        return ResponseEntity.ok(tempFilesService.getTempFiles(id));}

    @DeleteMapping(path="/TempFiles/{id}")
    public String deleteTempFiles(@PathVariable int id){
        tempFilesService.deleteTempFiles(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/TempFiles", method= RequestMethod.PUT)
    public ResponseEntity<?> updateTempFiles(@RequestBody TempFiles tempFiles){
        return ResponseEntity.ok(tempFilesService.updateTempFiles(tempFiles));
    }
}
