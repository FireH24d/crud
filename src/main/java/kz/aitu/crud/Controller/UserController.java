package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.User;
import kz.aitu.crud.Service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping(path="/User")
    public ResponseEntity<?> getAllUser(){
        return ResponseEntity.ok(userService.getAllUser());
    }
    @GetMapping(path="/User/{id}")
    public ResponseEntity<?> getUser(@PathVariable long id){
        return ResponseEntity.ok(userService.getUser(id));}

    @DeleteMapping(path="/User/{id}")
    public String deleteUser(@PathVariable int id){
        userService.deleteUser(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/User", method= RequestMethod.PUT)
    public ResponseEntity<?> updateUser(@RequestBody User user){
        return ResponseEntity.ok(userService.updateUser(user));
    }
}
