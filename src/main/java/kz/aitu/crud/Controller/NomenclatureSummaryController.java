package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Nomenclature;
import kz.aitu.crud.Entity.NomenclatureSummary;
import kz.aitu.crud.Service.NomenclatureService;
import kz.aitu.crud.Service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }
    @GetMapping(path="/NomenclatureSummary")
    public ResponseEntity<?> getAllNomenclatureSummary(){
        return ResponseEntity.ok(nomenclatureSummaryService.getAllNomenclatureSummary());
    }
    @GetMapping(path="/NomenclatureSummary/{id}")
    public ResponseEntity<?> getNomenclatureSummary(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.getNomenclatureSummary(id));}

    @DeleteMapping(path="/NomenclatureSummary/{id}")
    public String deleteNomenclatureSummary(@PathVariable int id){
        nomenclatureSummaryService.deleteNomenclatureSummary(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/NomenclatureSummary", method= RequestMethod.PUT)
    public ResponseEntity<?> updateNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok(nomenclatureSummaryService.updateNomenclatureSummary(nomenclatureSummary));
    }
}
