package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Share;
import kz.aitu.crud.Entity.StatusRequestHistory;
import kz.aitu.crud.Service.ShareService;
import kz.aitu.crud.Service.StatusRequestHistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class StatusRequestHistoryController {
    private final StatusRequestHistoryService statusRequestHistoryService;

    public StatusRequestHistoryController(StatusRequestHistoryService statusRequestHistoryService) {
        this.statusRequestHistoryService = statusRequestHistoryService;
    }
    @GetMapping(path="/StatusRequestHistory")
    public ResponseEntity<?> getAllStatusRequestHistory(){
        return ResponseEntity.ok(statusRequestHistoryService.getAllStatusRequestHistory());
    }
    @GetMapping(path="/StatusRequestHistory/{id}")
    public ResponseEntity<?> getStatusRequestHistory(@PathVariable long id){
        return ResponseEntity.ok(statusRequestHistoryService.getStatusRequestHistory(id));}

    @DeleteMapping(path="/StatusRequestHistory/{id}")
    public String deleteStatusRequestHistory(@PathVariable int id){
        statusRequestHistoryService.deleteStatusRequestHistory(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/StatusRequestHistory", method= RequestMethod.PUT)
    public ResponseEntity<?> updateStatusRequestHistory(@RequestBody StatusRequestHistory statusRequestHistory){
        return ResponseEntity.ok(statusRequestHistoryService.updateStatusRequestHistory(statusRequestHistory));
    }
}
