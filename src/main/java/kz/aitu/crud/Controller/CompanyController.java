package kz.aitu.crud.Controller;

import kz.aitu.crud.Entity.Company;
import kz.aitu.crud.Entity.CompanyUnit;
import kz.aitu.crud.Service.CompanyService;
import kz.aitu.crud.Service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
    @GetMapping(path="/Company")
    public ResponseEntity<?> getAllCompany(){
        return ResponseEntity.ok(companyService.getAllCompany());
    }
    @GetMapping(path="/Company/{id}")
    public ResponseEntity<?> getCompany(@PathVariable long id){
        return ResponseEntity.ok(companyService.getCompany(id));}

    @DeleteMapping(path="/Company/{id}")
    public String deleteCompany(@PathVariable int id){
        companyService.deleteCompany(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Company", method= RequestMethod.PUT)
    public ResponseEntity<?> updateCompany(@RequestBody Company company){
        return ResponseEntity.ok(companyService.updateCompany(company));
    }
}
